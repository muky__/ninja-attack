import SpriteKit

class GameOverScene : SKScene {
  init(size: CGSize, won : Bool, monsterKilled : Int) {
    super.init(size: size)
    
    backgroundColor = SKColor.black
    
    let message = won ? "You won, Congratulations!" : monsterKilled > 0 ? "You Lost, You could only stop \(monsterKilled) monsters" : "You Lost, You couldn't stop a single monster"
    
    let label = SKLabelNode()
    label.text = message
    label.fontSize = 25
    label.fontName = "AvenirNext-Bold"
    label.fontColor = SKColor.white
    label.position = CGPoint(x: size.width / 2, y: size.height / 2)
    addChild(label)
    
    run(SKAction.sequence([
        SKAction.wait(forDuration: 1.0),
        SKAction.run({[weak self] in
          guard let `self` = self else {
            return
          }
          
          let playAgainLabel = SKLabelNode()
          playAgainLabel.name = "playAgain"
          playAgainLabel.text = "Play Again"
          playAgainLabel.fontSize = 40
          playAgainLabel.fontName = "AvenirNext-Bold"
          playAgainLabel.fontColor = SKColor.white
          playAgainLabel.position = CGPoint(x: size.width / 2, y: label.yScale + label.frame.height)
          self.addChild(playAgainLabel)
          
          let mainMenuLabel = SKLabelNode()
          mainMenuLabel.name = "mainMenu"
          mainMenuLabel.text = "Main Menu"
          mainMenuLabel.fontSize = 40
          mainMenuLabel.fontName = "AvenirNext-Bold"
          mainMenuLabel.fontColor = SKColor.white
          mainMenuLabel.position = CGPoint(x: size.width / 2, y: size.height - mainMenuLabel.frame.height - 20)
          self.addChild(mainMenuLabel)
        })
      ]))
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    for touch in touches {
      let touchLocation = touch.location(in: self)
      if atPoint(touchLocation).name == "playAgain" {
          let transition = SKTransition.flipHorizontal(withDuration: 0.5)
          let scene = GameScene(size: size)
          self.view?.presentScene(scene, transition: transition)
      }
      else if atPoint(touchLocation).name == "mainMenu" {
        let transition = SKTransition.flipVertical(withDuration: 0.5)
        let scene = DifficultyMenu(size: size)
        self.view?.presentScene(scene, transition: transition)
      }
    }
  }
}
