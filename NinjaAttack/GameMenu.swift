import SpriteKit
import Foundation

class GameMenu : SKScene {
  override func didMove(to view: SKView) {
    
    let background = SKSpriteNode(imageNamed: "bg")
    background.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
    addChild(background)
    
    let startGameLabel = SKLabelNode(text: "Start Game")
    startGameLabel.fontSize = 40
    startGameLabel.fontColor = SKColor.black
    startGameLabel.fontName = "AvenirNext-Bold"
    startGameLabel.name = "startGame"
    startGameLabel.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
    startGameLabel.zPosition = 100
    addChild(startGameLabel)
  }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    
    let gameScene = GameScene(size: frame.size)
    gameScene.scaleMode = .aspectFill
    view?.presentScene(gameScene, transition: SKTransition.doorsOpenHorizontal(withDuration: 2))
//    for touch in touches {
//      let touchLocation = touch.location(in: self)
//      if atPoint(touchLocation).name == "startGame" {
//        let gameScene = GameScene(size: frame.size)
//        gameScene.scaleMode = .aspectFill
//        view?.presentScene(gameScene, transition: SKTransition.doorsOpenHorizontal(withDuration: 2))
//      }
//    }
  }
}

