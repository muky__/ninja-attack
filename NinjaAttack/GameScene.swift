import SpriteKit

func +(left: CGPoint, right: CGPoint) -> CGPoint {
  return CGPoint(x: left.x + right.x, y: left.y + right.y)
}

func -(left: CGPoint, right: CGPoint) -> CGPoint {
  return CGPoint(x: left.x - right.x, y: left.y - right.y)
}

func *(point: CGPoint, scalar: CGFloat) -> CGPoint {
  return CGPoint(x: point.x * scalar, y: point.y * scalar)
}

func /(point: CGPoint, scalar: CGFloat) -> CGPoint {
  return CGPoint(x: point.x / scalar, y: point.y / scalar)
}

#if !(arch(x86_64) || arch(arm64))
func sqrt(a: CGFloat) -> CGFloat {
  return CGFloat(sqrtf(Float(a)))
}
#endif

extension CGPoint {
  func length() -> CGFloat {
    return sqrt(x*x + y*y)
  }
  
  func normalized() -> CGPoint {
    return self / length()
  }
}

struct PhysicsCategory {
  static let none : UInt32 = 0
  static let all : UInt32 = UInt32.max
  static let monster : UInt32 = UInt32(1)
  static let blade : UInt32 = UInt32(2)
  static let player : UInt32 = UInt32(3)
}

class GameScene: SKScene, SKPhysicsContactDelegate {
  
  let player = SKSpriteNode(imageNamed: "player")
  
  //Node variable to pause certain nodes only
  var worldNode : SKNode!
  
  var scoreNode : SKLabelNode!
  var pauseNode : SKSpriteNode!
  var createMonsterAction : SKAction?
  
  var monstersDestroyed = 0
  var monstersCreated = 0
  var gamePauseState : Bool = false
  var difficultyLevel : String!
  
  var isScreenTouched : Bool = false
  var touchLocation : CGPoint = CGPoint(x: 0, y: 0)
  var count : Int = 19
  
  var knifeSound = SKAction.playSoundFileNamed("knife.mp3", waitForCompletion: false)
  var howlSound = SKAction.playSoundFileNamed("howl.mp3", waitForCompletion: false)
  
  //MARK:- Instantiate function
  override func didMove(to view: SKView) {
    worldNode = SKNode()
    addChild(worldNode)
    difficultyLevel = UserDefaults.standard.object(forKey: "difficultyLevel") as! String
    
    addScoreToScene()
    addPauseButtonToScene()
    
    let background = SKSpriteNode(imageNamed: "grass")
    background.size = frame.size
    background.zPosition = 1
    background.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
    addChild(background)
    
    let backgroundMusic = SKAudioNode(fileNamed: "background-music-aac.caf")
    backgroundMusic.autoplayLooped = true
    worldNode.addChild(backgroundMusic)
    
    player.zPosition = 50
    player.size = CGSize(width: 30, height: 60)
    player.physicsBody = SKPhysicsBody(texture: SKTexture(imageNamed: "player"), size: player.size)
    player.physicsBody?.isDynamic = true
    player.physicsBody?.categoryBitMask = PhysicsCategory.player
    player.physicsBody?.contactTestBitMask = PhysicsCategory.monster
    player.physicsBody?.collisionBitMask = PhysicsCategory.none
    player.position = CGPoint(x: size.width * 0.08, y: size.height * 0.5)
    worldNode.addChild(player)
    
    physicsWorld.gravity = .zero
    physicsWorld.contactDelegate = self
    
    switch difficultyLevel {
    case "easy":
      createMonster(minDuration: 3, maxDuration: 4, numberOfMonster: 20)
      break
    case "medium":
      createMonster(minDuration: 2, maxDuration: 4, numberOfMonster: 30)
      break
    default:
      createMonster(minDuration: 2, maxDuration: 3, numberOfMonster: 50)
    }
  }
  
  //MARK:- random num generator functions
  func random() -> CGFloat {
    return CGFloat(Float(arc4random()) / 0xFFFFFFFF)
  }
  
  func random(min : CGFloat, max : CGFloat) -> CGFloat {
    return random() * (max - min) + min
  }
  
  //MARK:- monster function
  func createMonster(minDuration : CGFloat, maxDuration : CGFloat, numberOfMonster : Int) {
    createMonsterAction = SKAction.repeatForever(SKAction.sequence([
      SKAction.run({
        if self.monstersCreated < numberOfMonster {
          self.addMonster(minDuration: minDuration,maxDuration: maxDuration)
        }
      }),
      SKAction.wait(forDuration: 1.5)
      ])
      )
    worldNode.run(createMonsterAction!)
  }
  
  func addMonster(minDuration : CGFloat, maxDuration : CGFloat) {
    let monster = generateRandomMonster()
    monster.zPosition = 50
    
    monster.physicsBody = SKPhysicsBody(rectangleOf: monster.size)
    monster.physicsBody?.isDynamic = true
    monster.physicsBody?.categoryBitMask = PhysicsCategory.monster
    monster.physicsBody?.contactTestBitMask = PhysicsCategory.blade
    monster.physicsBody?.collisionBitMask = PhysicsCategory.none
    
    let actualY = random(min: monster.size.height / 2, max: size.height - monster.size.height / 2)
    monster.position = CGPoint(x: size.width + monster.size.width / 2, y: actualY)
    worldNode.addChild(monster)
    
    //speed of monster
    let actualDuration = random(min: CGFloat(minDuration), max: CGFloat(maxDuration))
    
    //action
    let actionMove = SKAction.move(to: CGPoint(x: -monster.size.width / 2, y: actualY), duration: TimeInterval(actualDuration))
    let actionMoveDone = SKAction.removeFromParent()
    
    let loseAction = SKAction.run(){
      [weak self] in
      guard let `self` = self else { return }
      self.revealGameFinishedScene(won: false)
    }
    monster.run(SKAction.sequence([actionMove, loseAction, actionMoveDone]))
    monstersCreated += 1
  }
  
  func blinkMonsterWhenHit(monster : SKSpriteNode) {
    let action1 = SKAction.colorize(with: .cyan, colorBlendFactor: 1.0, duration: 0.2)
    let wait = SKAction.wait(forDuration: 0.1)
    let action2 = SKAction.colorize(withColorBlendFactor: 0.0, duration: 0.2)
    let actualY = monster.position.y
    monster.removeAllActions()
    
    //speed of monster
    let actualDuration = random(min: CGFloat(2.0), max: CGFloat(3.0))
    
    //action
    let actionMove = SKAction.move(to: CGPoint(x: -monster.size.width / 2, y: actualY), duration: TimeInterval(actualDuration))
    let actionMoveDone = SKAction.removeFromParent()
    
    let loseAction = SKAction.run(){
      [weak self] in
      guard let `self` = self else { return }
      self.revealGameFinishedScene(won: false)
    }
    monster.run(SKAction.sequence([action1, wait, action2, wait, actionMove, loseAction, actionMoveDone]))
  }
  
  func generateRandomMonster() -> SKSpriteNode {
    let monster : SKSpriteNode!
    let randomNumber = Helper().generateRandomNumber(firstNumber: 1, secondNumber: 16)
    let randomNum = Helper().generateRandomNumber(firstNumber: 40, secondNumber: 100)
    switch randomNumber {
    case 1...4:
      monster = SKSpriteNode(imageNamed: "greenMonster")
      monster.name = "greenMonster"
      break
    case 5...8:
      monster = SKSpriteNode(imageNamed: "purpleMonster")
      monster.name = "purpleMonster"
      break
    case 9...12:
      monster = SKSpriteNode(imageNamed: "pinkMonster")
      monster.name = "pinkMonster"
      break
    case 13...16:
      monster = SKSpriteNode(imageNamed: "orangeMonster")
      monster.name = "orangeMonster"
      break
    default:
      monster = SKSpriteNode(imageNamed: "greenMonster")
      monster.name = "greenMonster"
    }
    monster.size = CGSize(width: randomNum, height: randomNum)
    monster.physicsBody = SKPhysicsBody(texture: SKTexture(imageNamed: monster.name!), size: monster.size)
    return monster
  }
  
  //MARK:- touch functions
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    guard let touch = touches.first else {
      return
    }
    touchLocation = touch.location(in: self)
//    throwBlade(touchLocation: touchLocation)
    if atPoint(touchLocation).name == "pauseButton" {
      isScreenTouched = false
      pauseGame()
    }
    else if atPoint(touchLocation).name == "playAgain" {
      let transition = SKTransition.flipHorizontal(withDuration: 0.5)
      let scene = GameScene(size: size)
      self.view?.presentScene(scene, transition: transition)
    }
    else if atPoint(touchLocation).name == "mainMenu" {
      let transition = SKTransition.flipVertical(withDuration: 0.5)
      let scene = DifficultyMenu(size: size)
      self.view?.presentScene(scene, transition: transition)
    } else {
      isScreenTouched = true
    }
  }
  
  override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    guard let touch = touches.first else {
      return
    }
    touchLocation = touch.location(in: self)
//    throwBlade(touchLocation: touchLocation)
    isScreenTouched = true
  }
  
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    isScreenTouched = false
    count = 19
  }
  
  //MARK:- update frame function
  override func update(_ currentTime: TimeInterval) {
    
    updateScore()
    
    if isScreenTouched {
      count += 1
      if count == 20 {
        self.throwBlade(touchLocation: self.touchLocation)
        count = 0
      }
    }
  }
  
  //MARK:- pause function
  func addPauseButtonToScene() {
    pauseNode = SKSpriteNode(imageNamed: "pause")
    pauseNode.name = "pauseButton"
    pauseNode.size = CGSize(width: 30, height: 30)
    pauseNode.position = CGPoint(x: scoreNode.position.x, y: scoreNode.position.y - 20)
    pauseNode.zPosition = 11
    addChild(pauseNode)
  }
  
  //function that pauses the game
  func pauseGame()
  {
    gamePauseState = !gamePauseState
    if gamePauseState {
      pauseNode.texture = SKTexture(imageNamed: "play")
      worldNode.isPaused = true
    } else {
      pauseNode.texture = SKTexture(imageNamed: "pause")
      worldNode.isPaused = false
    }
  }

  //MARK:- blade functions
 func throwBlade(@objc touchLocation : CGPoint) {
    run(SKAction.playSoundFileNamed("pew-pew-lei.caf", waitForCompletion: false))

    let blade = SKSpriteNode(imageNamed: "projectile")
    //initial location of blade
    blade.size = CGSize(width: 30, height: 30)
    blade.zPosition = 50
    blade.position = player.position

    blade.physicsBody = SKPhysicsBody(circleOfRadius: blade.size.width / 2)
    blade.physicsBody?.isDynamic = true
    blade.physicsBody?.categoryBitMask = PhysicsCategory.blade
    blade.physicsBody?.contactTestBitMask = PhysicsCategory.monster
    blade.physicsBody?.collisionBitMask = PhysicsCategory.none
    blade.physicsBody?.usesPreciseCollisionDetection = true

    let offset = touchLocation - blade.position

    if offset.x < 0 {
      return
    }

    worldNode.addChild(blade)

    let direction = offset.normalized()
    let shootDistance = direction * 1000
    let realDestination = shootDistance + blade.position

    //blade action
    let actionMove = SKAction.move(to: realDestination, duration: 2.0)
    let actionMoveDone = SKAction.removeFromParent()
    let rotateAction = SKAction.rotate(byAngle: CGFloat(20 * Double.pi), duration: 3)
    blade.run(rotateAction)
    blade.run(SKAction.sequence([actionMove, actionMoveDone]))
  }
  
  //MARK:- sound function
  func playSound(sound : SKAction)
  {
    run(sound)
  }
  
  //MARK:- contact functions
  func bladeDidHitMonster(blade : SKSpriteNode, monster : SKSpriteNode) {
      playSound(sound: knifeSound)
      blade.removeFromParent()
      monster.removeFromParent()
    
      monstersDestroyed += 1
    
    switch difficultyLevel {
    case "easy":
      if monstersDestroyed == 20 {
        revealGameFinishedScene(won: true)
      }
    case "medium":
      if monstersDestroyed == 30 {
        revealGameFinishedScene(won: true)
      }
    default:
      if monstersDestroyed == 50 {
        revealGameFinishedScene(won: true)
      }
    }
  }
  
  func samuraiDidGetKilled(player : SKSpriteNode, monster : SKSpriteNode) {
    playSound(sound: howlSound)
    player.removeFromParent()
    monster.removeFromParent()
  }
  
  //MARK:- score functions
  func addScoreToScene() {
    scoreNode = SKLabelNode()
    scoreNode.text = "Score: 0"
    scoreNode.fontSize = 18
    scoreNode.fontName = "AvenirNext-Bold"
    scoreNode.fontColor = SKColor.white
    scoreNode.zPosition = 10
    scoreNode.position = CGPoint(x: frame.width - scoreNode.frame.width + 20, y: frame.height - 30)
    addChild(scoreNode)
  }
  
  func updateScore() {
    scoreNode?.text = "Score: \(monstersDestroyed)"
  }
  
  //MARK:- gameFinished scene
  func revealGameFinishedScene(won : Bool) {
//    self.worldNode.isPaused = true
//    blurWithCompletion()
    pauseGame()
    let overlay = SKSpriteNode(color: UIColor.black, size: self.frame.size)
    overlay.position = CGPoint(x: frame.width / 2, y: frame.height / 2)
    overlay.alpha = 0.7
    overlay.zPosition = 100
    self.addChild(overlay)
    showFinishedSceneNodes(won: won)
//    let reveal = SKTransition.flipHorizontal(withDuration: 0.5)
//    let gameOverScene = GameOverScene(size: self.size, won: won, monsterKilled: self.monstersDestroyed)
//    self.view?.presentScene(gameOverScene, transition: reveal)
  }
  
  func showFinishedSceneNodes(won: Bool) {
    run(SKAction.run({
      let message = won ? "You won, Congratulations!" : self.monstersDestroyed > 0 ? "You Lost, You could only stop \(self.monstersDestroyed) monsters" : "You Lost, You couldn't stop a single monster"
      
      let label = SKLabelNode()
      label.text = message
      label.fontSize = 25
      label.fontName = "AvenirNext-Bold"
      label.fontColor = SKColor.white
      label.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
      label.zPosition = 200
      self.addChild(label)
      
      self.run(SKAction.sequence([
        SKAction.wait(forDuration: 1.0),
        SKAction.run({[weak self] in
          guard let `self` = self else {
            return
          }
          
          let playAgainLabel = SKLabelNode()
          playAgainLabel.name = "playAgain"
          playAgainLabel.text = "Play Again"
          playAgainLabel.fontSize = 40
          playAgainLabel.fontName = "AvenirNext-Bold"
          playAgainLabel.fontColor = SKColor.white
          playAgainLabel.position = CGPoint(x: self.size.width / 2, y: label.yScale + label.frame.height)
          playAgainLabel.zPosition = 200
          self.addChild(playAgainLabel)
          
          let mainMenuLabel = SKLabelNode()
          mainMenuLabel.name = "mainMenu"
          mainMenuLabel.text = "Main Menu"
          mainMenuLabel.fontSize = 40
          mainMenuLabel.fontName = "AvenirNext-Bold"
          mainMenuLabel.fontColor = SKColor.white
          mainMenuLabel.position = CGPoint(x: self.size.width / 2, y: self.size.height - mainMenuLabel.frame.height - 20)
          mainMenuLabel.zPosition = 200
          self.addChild(mainMenuLabel)
        })
        ]))
    }))
  }
  
  //MARK:- blur function
  func blurWithCompletion() {
    let duration: CGFloat = 0.3
    let filter: CIFilter = CIFilter(name: "CIGaussianBlur", withInputParameters: ["inputRadius" : NSNumber(value:1.0)])!
    self.filter = filter
    self.shouldRasterize = true
    self.shouldEnableEffects = true
    run(SKAction.customAction(withDuration: 1, actionBlock: { (node: SKNode, elapsedTime: CGFloat) in
      let radius = (elapsedTime/duration) * 10.0
      (node as? SKEffectNode)!.filter!.setValue(radius, forKey: "inputRadius")
      self.worldNode.isPaused = true
    }))
  }
  
  //MARK:- SKPhysicsContactDelegate function
  func didBegin(_ contact: SKPhysicsContact) {
    var firstBody : SKPhysicsBody
    var secondBody : SKPhysicsBody
    if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
      firstBody = contact.bodyA
      secondBody = contact.bodyB
    }else {
      secondBody = contact.bodyA
      firstBody = contact.bodyB
    }
    
    if (firstBody.categoryBitMask == PhysicsCategory.monster) &&
      (secondBody.categoryBitMask == PhysicsCategory.blade) {
      if let monster = firstBody.node as? SKSpriteNode,
        let blade = secondBody.node as? SKSpriteNode {
          if monster.size.height > 70 {
            blinkMonsterWhenHit(monster: monster)
            monster.size = CGSize(width: 50, height: 50)
            blade.removeFromParent()
        }
        else {
          bladeDidHitMonster(blade: blade, monster: monster)
        }
      }
    }
    else if (firstBody.categoryBitMask == PhysicsCategory.monster) &&
      (secondBody.categoryBitMask == PhysicsCategory.player) {
      if let monster = firstBody.node as? SKSpriteNode,
        let player = secondBody.node as? SKSpriteNode {
          samuraiDidGetKilled(player: player, monster: monster)
          revealGameFinishedScene(won: false)
//          let reveal = SKTransition.flipHorizontal(withDuration: 0.5)
//          let gameOverScene = GameOverScene(size: self.size, won: false, monsterKilled: self.monstersDestroyed)
//          self.view?.presentScene(gameOverScene, transition: reveal)
      }
    }
  }
}
