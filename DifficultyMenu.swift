import Foundation
import SpriteKit

class DifficultyMenu : SKScene{
  var selectDifficultyLabel : SKLabelNode!
  var easyLevelButton : SKLabelNode!
  var mediumLevelButton : SKLabelNode!
  var hardLevelButton : SKLabelNode!
  
  override func didMove(to view: SKView) {
    
    backgroundColor = SKColor.white
    
    selectDifficultyLabel = SKLabelNode(text: "Select difficulty level")
    selectDifficultyLabel.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2 + 100)
    selectDifficultyLabel.fontSize = 50
    selectDifficultyLabel.fontColor = SKColor.black
    selectDifficultyLabel.fontName = "AvenirNext-Bold"
    selectDifficultyLabel.zPosition = 50
    
    addChild(selectDifficultyLabel)
    
    easyLevelButton = SKLabelNode(text: "Easy")
    easyLevelButton.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2 + 30)
    easyLevelButton.fontSize = 40
    easyLevelButton.fontColor = SKColor.black
    easyLevelButton.fontName = "AvenirNext-Bold"
    easyLevelButton.name = "easyLevelButton"
    easyLevelButton.zPosition = 50
    
    addChild(easyLevelButton)
    
    mediumLevelButton = SKLabelNode(text: "Medium")
    mediumLevelButton.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2 - 25)
    mediumLevelButton.fontSize = 40
    mediumLevelButton.fontColor = SKColor.black
    mediumLevelButton.fontName = "AvenirNext-Bold"
    mediumLevelButton.name = "mediumLevelButton"
    mediumLevelButton.zPosition = 50
    
    addChild(mediumLevelButton)
    
    hardLevelButton = SKLabelNode(text: "Hard")
    hardLevelButton.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2 - 80)
    hardLevelButton.fontSize = 40
    hardLevelButton.fontColor = SKColor.black
    hardLevelButton.fontName = "AvenirNext-Bold"
    hardLevelButton.zPosition = 50
    hardLevelButton.name = "hardLevelButton"
    
    addChild(hardLevelButton)
  }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    for touch in touches {
      let touchLocation = touch.location(in: self)
      if atPoint(touchLocation).name == "easyLevelButton" {
        UserDefaults.standard.set("easy", forKey: "difficultyLevel")
        beginGame()
      } else if atPoint(touchLocation).name == "mediumLevelButton" {
        UserDefaults.standard.set("medium", forKey: "difficultyLevel")
        beginGame()
      } else if atPoint(touchLocation).name == "hardLevelButton" {
        UserDefaults.standard.set("hard", forKey: "difficultyLevel")
        beginGame()
      }
    }
  }
  
  func beginGame() {
    let gameMenu = GameMenu(size: frame.size)
    gameMenu.scaleMode = .aspectFill
    view?.presentScene(gameMenu, transition: SKTransition.crossFade(withDuration: 1))
  }
}
