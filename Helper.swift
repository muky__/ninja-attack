import Foundation
import UIKit

class Helper {
  func generateRandomNumber(firstNumber : CGFloat, secondNumber : CGFloat) -> CGFloat {
    return CGFloat(arc4random()) / CGFloat(UINT32_MAX) * abs(firstNumber - secondNumber) + min(firstNumber, secondNumber)
  }
}
